- There are Nutrients like Nitrogen, Phosphorus, Potassium.
-
- #Nitrogen
	- Nitrogen gives plant leaves their dark green color and promotes growth.
		- Plants use nitrogen to build amino acids, which are the building blocks of proteins.
- #Phosphorus
	- encourages cell division, helps root growth, protects plants from disease, and allows plants to produce flowers and seeds.
		- Phosphorus is necessary for building up nucleic acid structures that regulate protein synthesis
- #Potassium
	- protects plants from diseases and encourages root growth. Potassium is also necessary for plants to make chlorophyll.
		- Chlorophyll is the green pigment responsible for providing energy to plants by absorbing energy from sunlight.
		- They use 
		  potassium to help open and close their stomata, which are similar to 
		  pores. Stomata allow plants to intake CO2 and build ATP, one of the 
		  basic energy units needed for life.
-
- **Deficiencies**
- #Nitrogen
	- Plants with a nitrogen deficiency will grow smaller and leaves or foliage will turn yellow and may also appear thin or pale.
	-
- #Phosphorus
	-
-